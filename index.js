const express = require("express");
const cors = require("cors")
const mongoose = require("mongoose");
const {
  MONGO_USER,
  MONGO_PASS,
  MONGO_IP,
  MONGO_PORT,
  REDIS_URL,
  REDIS_SESSION_SECRET,
  REDIS_PORT,
} = require("./config/config");
const port = process.env.port || 3000;
const postRouter = require("./routes/postRoutes");
const userRouter = require("./routes/userRoutes");
const mongoURL = `mongodb://${MONGO_USER}:${MONGO_PASS}@${MONGO_IP}:${MONGO_PORT}/?authSource=admin`;
// const localMongoURL = `mongodb://localhost:27017/?authSource=admin`
const redis = require("redis");
const session = require("express-session");
// const RedisStore = require("connect-redis")(session)
const app = express();
app.use(cors({}));


// Initialize client.
let redisClient = redis.createClient({
  host: REDIS_URL,
  port: REDIS_PORT,
});

app.enable("trust proxy")

// Initialize sesssion storage.
app.use(
  session({
    // store: new RedisStore(redisClient),
    resave: false, // required: force lightweight session keep alive (touch)
    saveUninitialized: false, // recommended: only save session when data exists
    secret: REDIS_SESSION_SECRET,
    cookie: {
      secure: false,
      resave: false,
      saveUninitialized: false,
      httpOnly: true,
      maxAge: 30000,
    },
  })
);

app.use(express.json());

mongoose
  .connect(mongoURL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("Database connected successfully");
  })
  .catch((e) => {
    console.log(e);
  });

app.use("/api/v1/posts", postRouter);
app.use("/api/v1/user", userRouter);

app.listen(port, () => {
  console.log(`Server started running on port ${port}`);
});
