const User = require("../models/userModel");
const bcrypt = require("bcrypt");

exports.signup = async (req, res, next) => {
  try {
    req.body.password = await bcrypt.hash(req.body.password, 12);
    const user = await User.create(req.body);

    res.status(200).json({
      result: "success",
      response: "User registered successfully",
      data: {
        user: user,
      },
    });
  } catch (error) {
    res.status(400).json({
      error: "Failed to register user",
    });
  }
};

exports.login = async (req, res, next) => {
  try {
    const { username, password } = req.body;
    const user = await User.findOne({
      username: username,
    });

    if (user) {
      const checkPassword = await bcrypt.compare(password, user.password);
      if (checkPassword) {
        console.log("Login successfully")
        res.status(200).json({
          result: "Login successfully",
        });
      } else {
        res.status(200).json({
          result: "success",
          response: "User not registered with us",
        });
      }
    } else {
      res.status(200).json({
        result: "success",
        response: "User not registered with us",
      });
    }
  } catch (error) {
    res.status(400).json({
      error: "Failed to login user",
    });
  }
};
