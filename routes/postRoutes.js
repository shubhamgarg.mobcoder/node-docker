const express = require("express");
const postController = require("../controllers/postController")
const router = express.Router();

router.route("/get-all").get(postController.getallPosts);
router.route("/add").post(postController.createPost);
// router.route("/get/:id").get(postController.getOnePost);
// router.route("update/:id").put(postController.updatePost);
// router.route("/delete/:id").delete(postController.deletePost)

router.route("/:id").get(postController.getOnePost).patch(postController.updatePost).delete(postController.deletePost);

module.exports = router